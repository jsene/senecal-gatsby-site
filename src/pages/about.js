import React from "react"
import { useStaticQuery, graphql, Link } from "gatsby"
import Image from "gatsby-image"

import Layout from "../components/layout"
import { rhythm } from "../utils/typography"

const About = (location) => {
    const data = useStaticQuery(graphql`
    query AboutQuery {
      avatar: file(absolutePath: { regex: "/profile-pic.jpg/" }) {
        childImageSharp {
          fixed(width: 200, height: 200) {
            ...GatsbyImageSharpFixed
          }
        }
      }
      site {
        siteMetadata {
          title
          author {
            name
            summary
          }
          social {
            twitter
          }
          menuLinks {
            name
            link
          }
        }
      }
    }
  `)

  const { title, author, menuLinks, social } = data.site.siteMetadata
    return (
        <Layout location={location} title={title} menuLinks={menuLinks}>
            <div
                style={{
                    display: `flex`,
                    marginTop: rhythm(3),
                }}
            >
                <Image
                    fixed={data.avatar.childImageSharp.fixed}
                    alt={author.name}
                    style={{
                        margin: `0 auto`,
                        marginBottom: rhythm(1),
                        minWidth: 200,
                        borderRadius: `100%`,
                    }}
                    imgStyle={{
                        borderRadius: `50%`,
                    }}
                />
            </div>
            <p>
              I am currently a software engineer at Figure Technologies. Figure is a financial 
              technology company leveraging blockchain, AI and analytics to deliver products to 
              improve the financial lives of our customers. We create innovative products for 
              consumers and a blockchain protocol that enhances the origination, custody, trading
              and securitization of assets for institutions.
              </p>
            <p>
              I completed my Bachelor's and Master's degrees at Montana State University in 
              mechanical engineering and computer science respectively. During my graduate 
              program I was a member of the Numerical Intelligent Systems Lab where we focus 
              on applying machine learning, mathematics, and software engineering to solve a 
              variety of problems. My research focused on developing techniques to assess the 
              health of large volumes of fruits and vegetables using a combination of hyper-spectral imaging 
              and machine learning.
            </p>
            <p>
              In the past I have worked at Los Alamos National Laboratory, where I applied 
              computational techniques to monitor the health of a variety of structures. 
              I also spent time working in the computational fluid dynamics field developing new
              algorithms and visualizations. Additionally, I spent time as an intern at Blackmore Sensors & Analytics 
              (now part of Aurora), where I developed software for automotive lidar systems.
            </p>
        </Layout>
    )
}

export default About 